<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>webserver</title>
    </head>
    <body>
        <p>Serveur installé.</p>
        <p>Server IP: <?= $_SERVER["SERVER_ADDR"]; ?></p>
        <p>Forwarded IP: <?= $_SERVER["HTTP_X_FORWARDED_FOR"]; ?>
        <p>Remote IP: <?= $_SERVER["REMOTE_ADDR"]; ?></p>
    </body>
</html>