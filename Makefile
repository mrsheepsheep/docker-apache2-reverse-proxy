install:
	docker-compose up --build -d

start:
	docker-compose up -d

sh_webserver:
	docker-compose exec webserver bash

sh_reverseproxy:
	docker-compose exec reverseproxy bash
